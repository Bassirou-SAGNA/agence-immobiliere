-- MariaDB dump 10.19  Distrib 10.4.32-MariaDB, for Win64 (AMD64)
--
-- Host: localhost    Database: db_agence_immobiliere1
-- ------------------------------------------------------
-- Server version	10.4.32-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `adresse`
--

DROP TABLE IF EXISTS `adresse`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `adresse` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code_postal` varchar(255) DEFAULT NULL,
  `ville` varchar(255) NOT NULL,
  `pays` varchar(255) NOT NULL,
  `user_create` varchar(255) DEFAULT NULL,
  `user_update` varchar(255) DEFAULT NULL,
  `user_delete` varchar(255) DEFAULT NULL,
  `date_create` datetime DEFAULT NULL,
  `date_update` datetime DEFAULT NULL,
  `date_delete` datetime DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `adresse`
--

LOCK TABLES `adresse` WRITE;
/*!40000 ALTER TABLE `adresse` DISABLE KEYS */;
/*!40000 ALTER TABLE `adresse` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bien_immobilier`
--

DROP TABLE IF EXISTS `bien_immobilier`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bien_immobilier` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `adresse_id` int(11) DEFAULT NULL,
  `categorie_id` int(11) DEFAULT NULL,
  `type_immobilier_id` int(11) DEFAULT NULL,
  `description` longtext DEFAULT NULL,
  `prix` double NOT NULL,
  `superficie` int(11) NOT NULL,
  `nombre_pieces` int(11) DEFAULT NULL,
  `nom_immobilier` varchar(255) NOT NULL,
  `user_create` varchar(255) DEFAULT NULL,
  `user_update` varchar(255) DEFAULT NULL,
  `user_delete` varchar(255) DEFAULT NULL,
  `date_create` datetime DEFAULT NULL,
  `date_update` datetime DEFAULT NULL,
  `date_delete` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_D1BE34E11562A4A5` (`nom_immobilier`),
  KEY `IDX_D1BE34E14DE7DC5C` (`adresse_id`),
  KEY `IDX_D1BE34E1BCF5E72D` (`categorie_id`),
  KEY `IDX_D1BE34E12C962E9C` (`type_immobilier_id`),
  CONSTRAINT `FK_D1BE34E12C962E9C` FOREIGN KEY (`type_immobilier_id`) REFERENCES `type_immobilier` (`id`),
  CONSTRAINT `FK_D1BE34E14DE7DC5C` FOREIGN KEY (`adresse_id`) REFERENCES `adresse` (`id`),
  CONSTRAINT `FK_D1BE34E1BCF5E72D` FOREIGN KEY (`categorie_id`) REFERENCES `categorie` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bien_immobilier`
--

LOCK TABLES `bien_immobilier` WRITE;
/*!40000 ALTER TABLE `bien_immobilier` DISABLE KEYS */;
/*!40000 ALTER TABLE `bien_immobilier` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categorie`
--

DROP TABLE IF EXISTS `categorie`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categorie` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom_categorie` varchar(255) DEFAULT NULL,
  `user_create` varchar(255) DEFAULT NULL,
  `user_update` varchar(255) DEFAULT NULL,
  `user_delete` varchar(255) DEFAULT NULL,
  `date_create` datetime DEFAULT NULL,
  `date_update` datetime DEFAULT NULL,
  `date_delete` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categorie`
--

LOCK TABLES `categorie` WRITE;
/*!40000 ALTER TABLE `categorie` DISABLE KEYS */;
/*!40000 ALTER TABLE `categorie` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contrat`
--

DROP TABLE IF EXISTS `contrat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contrat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `duree_contrat_id` int(11) DEFAULT NULL,
  `bien_immobilier_id` int(11) DEFAULT NULL,
  `client_id` int(11) DEFAULT NULL,
  `montant` double DEFAULT NULL,
  `nom_contrat` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `date_contrat` datetime NOT NULL,
  `user_create` varchar(255) DEFAULT NULL,
  `user_update` varchar(255) DEFAULT NULL,
  `user_delete` varchar(255) DEFAULT NULL,
  `date_create` datetime DEFAULT NULL,
  `date_update` datetime DEFAULT NULL,
  `date_delete` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_603499938CD3AC44` (`duree_contrat_id`),
  KEY `IDX_603499935992120A` (`bien_immobilier_id`),
  KEY `IDX_6034999319EB6921` (`client_id`),
  CONSTRAINT `FK_6034999319EB6921` FOREIGN KEY (`client_id`) REFERENCES `personne` (`id`),
  CONSTRAINT `FK_603499935992120A` FOREIGN KEY (`bien_immobilier_id`) REFERENCES `bien_immobilier` (`id`),
  CONSTRAINT `FK_603499938CD3AC44` FOREIGN KEY (`duree_contrat_id`) REFERENCES `duree_contrat` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contrat`
--

LOCK TABLES `contrat` WRITE;
/*!40000 ALTER TABLE `contrat` DISABLE KEYS */;
/*!40000 ALTER TABLE `contrat` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `duree_contrat`
--

DROP TABLE IF EXISTS `duree_contrat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `duree_contrat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `duree` int(11) NOT NULL,
  `user_create` varchar(255) DEFAULT NULL,
  `user_update` varchar(255) DEFAULT NULL,
  `user_delete` varchar(255) DEFAULT NULL,
  `date_create` datetime DEFAULT NULL,
  `date_update` datetime DEFAULT NULL,
  `date_delete` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `duree_contrat`
--

LOCK TABLES `duree_contrat` WRITE;
/*!40000 ALTER TABLE `duree_contrat` DISABLE KEYS */;
/*!40000 ALTER TABLE `duree_contrat` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `facture`
--

DROP TABLE IF EXISTS `facture`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facture` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) DEFAULT NULL,
  `contrat_id` int(11) DEFAULT NULL,
  `paiement_id` int(11) DEFAULT NULL,
  `num_facture` varchar(255) DEFAULT NULL,
  `montant` double NOT NULL,
  `date_emission` datetime NOT NULL,
  `date_paiement` datetime NOT NULL,
  `user_create` varchar(255) DEFAULT NULL,
  `user_update` varchar(255) DEFAULT NULL,
  `user_delete` varchar(255) DEFAULT NULL,
  `date_create` datetime DEFAULT NULL,
  `date_update` datetime DEFAULT NULL,
  `date_delete` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_FE86641019EB6921` (`client_id`),
  KEY `IDX_FE8664101823061F` (`contrat_id`),
  KEY `IDX_FE8664102A4C4478` (`paiement_id`),
  CONSTRAINT `FK_FE8664101823061F` FOREIGN KEY (`contrat_id`) REFERENCES `contrat` (`id`),
  CONSTRAINT `FK_FE86641019EB6921` FOREIGN KEY (`client_id`) REFERENCES `personne` (`id`),
  CONSTRAINT `FK_FE8664102A4C4478` FOREIGN KEY (`paiement_id`) REFERENCES `paiement` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `facture`
--

LOCK TABLES `facture` WRITE;
/*!40000 ALTER TABLE `facture` DISABLE KEYS */;
/*!40000 ALTER TABLE `facture` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `messenger_messages`
--

DROP TABLE IF EXISTS `messenger_messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `messenger_messages` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `body` longtext NOT NULL,
  `headers` longtext NOT NULL,
  `queue_name` varchar(190) NOT NULL,
  `created_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  `available_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  `delivered_at` datetime DEFAULT NULL COMMENT '(DC2Type:datetime_immutable)',
  PRIMARY KEY (`id`),
  KEY `IDX_75EA56E0FB7336F0` (`queue_name`),
  KEY `IDX_75EA56E0E3BD61CE` (`available_at`),
  KEY `IDX_75EA56E016BA31DB` (`delivered_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `messenger_messages`
--

LOCK TABLES `messenger_messages` WRITE;
/*!40000 ALTER TABLE `messenger_messages` DISABLE KEYS */;
/*!40000 ALTER TABLE `messenger_messages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `moyen_paiement`
--

DROP TABLE IF EXISTS `moyen_paiement`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `moyen_paiement` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type_paiement_id` int(11) DEFAULT NULL,
  `nom_moyen_paiement` varchar(255) DEFAULT NULL,
  `frais` double NOT NULL,
  `user_create` varchar(255) DEFAULT NULL,
  `user_update` varchar(255) DEFAULT NULL,
  `user_delete` varchar(255) DEFAULT NULL,
  `date_create` datetime DEFAULT NULL,
  `date_update` datetime DEFAULT NULL,
  `date_delete` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_ED4417D2615593E9` (`type_paiement_id`),
  CONSTRAINT `FK_ED4417D2615593E9` FOREIGN KEY (`type_paiement_id`) REFERENCES `type_paiement` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `moyen_paiement`
--

LOCK TABLES `moyen_paiement` WRITE;
/*!40000 ALTER TABLE `moyen_paiement` DISABLE KEYS */;
/*!40000 ALTER TABLE `moyen_paiement` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `paiement`
--

DROP TABLE IF EXISTS `paiement`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `paiement` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `moyen_paiement_id` int(11) DEFAULT NULL,
  `date_paiement` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_B1DC7A1E9C7E259C` (`moyen_paiement_id`),
  CONSTRAINT `FK_B1DC7A1E9C7E259C` FOREIGN KEY (`moyen_paiement_id`) REFERENCES `moyen_paiement` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `paiement`
--

LOCK TABLES `paiement` WRITE;
/*!40000 ALTER TABLE `paiement` DISABLE KEYS */;
/*!40000 ALTER TABLE `paiement` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `personne`
--

DROP TABLE IF EXISTS `personne`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `personne` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `adresse_id` int(11) DEFAULT NULL,
  `immobilier_client_id_id` int(11) DEFAULT NULL,
  `bienimmobilier_id` int(11) DEFAULT NULL,
  `nom` varchar(255) NOT NULL,
  `prenom` varchar(255) NOT NULL,
  `numero_telephone` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `user_create` varchar(255) DEFAULT NULL,
  `user_update` varchar(255) DEFAULT NULL,
  `user_delete` varchar(255) DEFAULT NULL,
  `date_create` datetime DEFAULT NULL,
  `date_update` datetime DEFAULT NULL,
  `date_delete` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_FCEC9EF4DE7DC5C` (`adresse_id`),
  KEY `IDX_FCEC9EF37B4C4A1` (`immobilier_client_id_id`),
  KEY `IDX_FCEC9EFEFFC4BC7` (`bienimmobilier_id`),
  CONSTRAINT `FK_FCEC9EF37B4C4A1` FOREIGN KEY (`immobilier_client_id_id`) REFERENCES `bien_immobilier` (`id`),
  CONSTRAINT `FK_FCEC9EF4DE7DC5C` FOREIGN KEY (`adresse_id`) REFERENCES `adresse` (`id`),
  CONSTRAINT `FK_FCEC9EFEFFC4BC7` FOREIGN KEY (`bienimmobilier_id`) REFERENCES `bien_immobilier` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `personne`
--

LOCK TABLES `personne` WRITE;
/*!40000 ALTER TABLE `personne` DISABLE KEYS */;
/*!40000 ALTER TABLE `personne` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rendez_vous`
--

DROP TABLE IF EXISTS `rendez_vous`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rendez_vous` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `agent_id` int(11) DEFAULT NULL,
  `client_id` int(11) DEFAULT NULL,
  `date` datetime NOT NULL,
  `commentaire` longtext DEFAULT NULL,
  `user_create` varchar(255) DEFAULT NULL,
  `user_update` varchar(255) DEFAULT NULL,
  `user_delete` varchar(255) DEFAULT NULL,
  `date_create` datetime DEFAULT NULL,
  `date_update` datetime DEFAULT NULL,
  `date_delete` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_65E8AA0A3414710B` (`agent_id`),
  KEY `IDX_65E8AA0A19EB6921` (`client_id`),
  CONSTRAINT `FK_65E8AA0A19EB6921` FOREIGN KEY (`client_id`) REFERENCES `personne` (`id`),
  CONSTRAINT `FK_65E8AA0A3414710B` FOREIGN KEY (`agent_id`) REFERENCES `personne` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rendez_vous`
--

LOCK TABLES `rendez_vous` WRITE;
/*!40000 ALTER TABLE `rendez_vous` DISABLE KEYS */;
/*!40000 ALTER TABLE `rendez_vous` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `type_immobilier`
--

DROP TABLE IF EXISTS `type_immobilier`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `type_immobilier` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom_type_immobilier` varchar(255) DEFAULT NULL,
  `user_create` varchar(255) DEFAULT NULL,
  `user_update` varchar(255) DEFAULT NULL,
  `user_delete` varchar(255) DEFAULT NULL,
  `date_create` datetime DEFAULT NULL,
  `date_update` datetime DEFAULT NULL,
  `date_delete` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `type_immobilier`
--

LOCK TABLES `type_immobilier` WRITE;
/*!40000 ALTER TABLE `type_immobilier` DISABLE KEYS */;
/*!40000 ALTER TABLE `type_immobilier` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `type_paiement`
--

DROP TABLE IF EXISTS `type_paiement`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `type_paiement` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `moyen_paiement_type_paiement_id_id` int(11) DEFAULT NULL,
  `nom_type_paiement` varchar(255) DEFAULT NULL,
  `user_create` varchar(255) DEFAULT NULL,
  `user_update` varchar(255) DEFAULT NULL,
  `user_delete` varchar(255) DEFAULT NULL,
  `date_create` datetime DEFAULT NULL,
  `date_update` datetime DEFAULT NULL,
  `date_delete` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_7C235DE2BAEC6E6C` (`moyen_paiement_type_paiement_id_id`),
  CONSTRAINT `FK_7C235DE2BAEC6E6C` FOREIGN KEY (`moyen_paiement_type_paiement_id_id`) REFERENCES `moyen_paiement` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `type_paiement`
--

LOCK TABLES `type_paiement` WRITE;
/*!40000 ALTER TABLE `type_paiement` DISABLE KEYS */;
/*!40000 ALTER TABLE `type_paiement` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `agent_id` int(11) DEFAULT NULL,
  `username` varchar(180) NOT NULL,
  `roles` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '(DC2Type:json)' CHECK (json_valid(`roles`)),
  `password` varchar(255) NOT NULL,
  `user_create` varchar(255) DEFAULT NULL,
  `user_update` varchar(255) DEFAULT NULL,
  `user_delete` varchar(255) DEFAULT NULL,
  `date_create` datetime DEFAULT NULL,
  `date_update` datetime DEFAULT NULL,
  `date_delete` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_8D93D6493414710B` (`agent_id`),
  CONSTRAINT `FK_8D93D6493414710B` FOREIGN KEY (`agent_id`) REFERENCES `personne` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (3,NULL,'papa','[]','$2y$13$vkqhig6CHriSn52z9iwbaO6qnYnVGwMfatjtFTcP85.zaOSvJIUQa','BSagna',NULL,NULL,'2024-08-05 05:03:27',NULL,NULL),(4,NULL,'Test','[]','$2y$13$CgkAvnI2/SUpVtjsN.WR9u2gWcoyENnaQi/HtDlD.44ARXxP6R4Qy','papa',NULL,NULL,'2024-08-06 17:28:49',NULL,NULL);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `visite`
--

DROP TABLE IF EXISTS `visite`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `visite` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `agent_id` int(11) DEFAULT NULL,
  `client_id` int(11) DEFAULT NULL,
  `bien_immobilier_id` int(11) DEFAULT NULL,
  `date` datetime NOT NULL,
  `commentaire` longtext DEFAULT NULL,
  `user_create` varchar(255) DEFAULT NULL,
  `user_update` varchar(255) DEFAULT NULL,
  `user_delete` varchar(255) DEFAULT NULL,
  `date_create` datetime DEFAULT NULL,
  `date_update` datetime DEFAULT NULL,
  `date_delete` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_B09C8CBB3414710B` (`agent_id`),
  KEY `IDX_B09C8CBB19EB6921` (`client_id`),
  KEY `IDX_B09C8CBB5992120A` (`bien_immobilier_id`),
  CONSTRAINT `FK_B09C8CBB19EB6921` FOREIGN KEY (`client_id`) REFERENCES `personne` (`id`),
  CONSTRAINT `FK_B09C8CBB3414710B` FOREIGN KEY (`agent_id`) REFERENCES `personne` (`id`),
  CONSTRAINT `FK_B09C8CBB5992120A` FOREIGN KEY (`bien_immobilier_id`) REFERENCES `bien_immobilier` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `visite`
--

LOCK TABLES `visite` WRITE;
/*!40000 ALTER TABLE `visite` DISABLE KEYS */;
/*!40000 ALTER TABLE `visite` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2024-08-06 17:33:38
