<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240705153719 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE acquisition (id INT AUTO_INCREMENT NOT NULL, montant DOUBLE PRECISION NOT NULL, date_acquisition DATETIME NOT NULL, user_create VARCHAR(255) DEFAULT NULL, user_update VARCHAR(255) DEFAULT NULL, user_delete VARCHAR(255) DEFAULT NULL, date_create DATETIME DEFAULT NULL, date_update DATETIME DEFAULT NULL, date_delete DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE adresse (id INT AUTO_INCREMENT NOT NULL, adresse_client_id_id INT DEFAULT NULL, adresse_agent_id_id INT DEFAULT NULL, immobilier_adresse_id_id INT DEFAULT NULL, code_postal VARCHAR(255) DEFAULT NULL, ville VARCHAR(255) NOT NULL, pays VARCHAR(255) NOT NULL, user_create VARCHAR(255) DEFAULT NULL, user_update VARCHAR(255) DEFAULT NULL, user_delete VARCHAR(255) DEFAULT NULL, date_create DATETIME DEFAULT NULL, date_update DATETIME DEFAULT NULL, date_delete DATETIME DEFAULT NULL, status TINYINT(1) NOT NULL, INDEX IDX_C35F0816EC52F3F0 (adresse_client_id_id), INDEX IDX_C35F081688E44FC0 (adresse_agent_id_id), INDEX IDX_C35F0816113403A1 (immobilier_adresse_id_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE agent (id INT AUTO_INCREMENT NOT NULL, agent_prise_service_id_id INT DEFAULT NULL, user_create VARCHAR(255) DEFAULT NULL, user_update VARCHAR(255) DEFAULT NULL, user_delete VARCHAR(255) DEFAULT NULL, date_create DATETIME DEFAULT NULL, date_update DATETIME DEFAULT NULL, date_delete DATETIME DEFAULT NULL, INDEX IDX_268B9C9DA148D5EF (agent_prise_service_id_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE categorie (id INT AUTO_INCREMENT NOT NULL, immobilier_categorie_id_id INT DEFAULT NULL, nom_categorie VARCHAR(255) DEFAULT NULL, user_create VARCHAR(255) DEFAULT NULL, user_update VARCHAR(255) DEFAULT NULL, user_delete VARCHAR(255) DEFAULT NULL, date_create DATETIME DEFAULT NULL, date_update DATETIME DEFAULT NULL, date_delete DATETIME DEFAULT NULL, INDEX IDX_497DD634E87A0BAA (immobilier_categorie_id_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE client (id INT AUTO_INCREMENT NOT NULL, contrat_client_id_id INT DEFAULT NULL, acquisition_client_id_id INT DEFAULT NULL, facture_client_id_id INT DEFAULT NULL, immobilier_client_id_id INT DEFAULT NULL, nom VARCHAR(255) DEFAULT NULL, prenom VARCHAR(255) DEFAULT NULL, telephone VARCHAR(255) DEFAULT NULL, email VARCHAR(255) DEFAULT NULL, user_create VARCHAR(255) DEFAULT NULL, user_update VARCHAR(255) DEFAULT NULL, user_delete VARCHAR(255) DEFAULT NULL, date_create DATETIME DEFAULT NULL, date_update DATETIME DEFAULT NULL, date_delete DATETIME DEFAULT NULL, INDEX IDX_C744045597F70B29 (contrat_client_id_id), INDEX IDX_C7440455EA5C1F0A (acquisition_client_id_id), INDEX IDX_C744045588F0D212 (facture_client_id_id), INDEX IDX_C744045537B4C4A1 (immobilier_client_id_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE contrat (id INT AUTO_INCREMENT NOT NULL, ligne_contrat_id_id INT DEFAULT NULL, facture_contrat_id_id INT DEFAULT NULL, montant DOUBLE PRECISION NOT NULL, nom_contrat VARCHAR(255) NOT NULL, description VARCHAR(255) NOT NULL, date_contrat DATETIME NOT NULL, user_create VARCHAR(255) DEFAULT NULL, user_update VARCHAR(255) DEFAULT NULL, user_delete VARCHAR(255) DEFAULT NULL, date_create DATETIME DEFAULT NULL, date_update DATETIME DEFAULT NULL, date_delete DATETIME DEFAULT NULL, INDEX IDX_603499938E65F6F0 (ligne_contrat_id_id), INDEX IDX_60349993D6E1BD71 (facture_contrat_id_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE depense (id INT AUTO_INCREMENT NOT NULL, ligne_depense_depense_id_id INT DEFAULT NULL, montant DOUBLE PRECISION NOT NULL, date_depense DATETIME NOT NULL, description VARCHAR(255) DEFAULT NULL, user_create VARCHAR(255) DEFAULT NULL, user_update VARCHAR(255) DEFAULT NULL, user_delete VARCHAR(255) DEFAULT NULL, date_create DATETIME DEFAULT NULL, date_update DATETIME DEFAULT NULL, date_delete DATETIME DEFAULT NULL, INDEX IDX_34059757547300E6 (ligne_depense_depense_id_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE duree_contrat (id INT AUTO_INCREMENT NOT NULL, contrat_duree_contrat_id_id INT DEFAULT NULL, duree_en_mois DOUBLE PRECISION NOT NULL, user_create VARCHAR(255) DEFAULT NULL, user_update VARCHAR(255) DEFAULT NULL, user_delete VARCHAR(255) DEFAULT NULL, date_create DATETIME DEFAULT NULL, date_update DATETIME DEFAULT NULL, date_delete DATETIME DEFAULT NULL, INDEX IDX_8BFD47C48E005BF (contrat_duree_contrat_id_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE facture (id INT AUTO_INCREMENT NOT NULL, num_facture VARCHAR(255) DEFAULT NULL, montant DOUBLE PRECISION NOT NULL, date_emission DATETIME NOT NULL, date_paiement DATETIME NOT NULL, user_create VARCHAR(255) DEFAULT NULL, user_update VARCHAR(255) DEFAULT NULL, user_delete VARCHAR(255) DEFAULT NULL, date_create DATETIME DEFAULT NULL, date_update DATETIME DEFAULT NULL, date_delete DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE immobilier (id INT AUTO_INCREMENT NOT NULL, contrat_immobilier_id_id INT DEFAULT NULL, promotion_immobilier_id_id INT DEFAULT NULL, description VARCHAR(255) DEFAULT NULL, prix DOUBLE PRECISION NOT NULL, surperficie INT NOT NULL, nbre_pieces INT NOT NULL, status ENUM(\'vendu\', \'louer\', \'acheter\'), user_create VARCHAR(255) DEFAULT NULL, user_update VARCHAR(255) DEFAULT NULL, user_delete VARCHAR(255) DEFAULT NULL, date_create DATETIME DEFAULT NULL, date_update DATETIME DEFAULT NULL, date_delete DATETIME DEFAULT NULL, INDEX IDX_142D24D22D9DD0EE (contrat_immobilier_id_id), INDEX IDX_142D24D238C55694 (promotion_immobilier_id_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ligne_contrat (id INT AUTO_INCREMENT NOT NULL, description VARCHAR(255) DEFAULT NULL, user_create VARCHAR(255) DEFAULT NULL, user_update VARCHAR(255) DEFAULT NULL, user_delete VARCHAR(255) DEFAULT NULL, date_create DATETIME DEFAULT NULL, date_update DATETIME DEFAULT NULL, date_delete DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ligne_depense (id INT AUTO_INCREMENT NOT NULL, description VARCHAR(255) DEFAULT NULL, user_create VARCHAR(255) DEFAULT NULL, user_update VARCHAR(255) DEFAULT NULL, user_delete VARCHAR(255) DEFAULT NULL, date_create DATETIME DEFAULT NULL, date_update DATETIME DEFAULT NULL, date_delete DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE moyen_paiement (id INT AUTO_INCREMENT NOT NULL, nom_moyen_paiement VARCHAR(255) DEFAULT NULL, frais DOUBLE PRECISION NOT NULL, user_create VARCHAR(255) DEFAULT NULL, user_update VARCHAR(255) DEFAULT NULL, user_delete VARCHAR(255) DEFAULT NULL, date_create DATETIME DEFAULT NULL, date_update DATETIME DEFAULT NULL, date_delete DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE notification (id INT AUTO_INCREMENT NOT NULL, type VARCHAR(255) DEFAULT NULL, message VARCHAR(255) DEFAULT NULL, statut VARCHAR(255) DEFAULT NULL, date_envoie DATETIME NOT NULL, user_create VARCHAR(255) DEFAULT NULL, user_update VARCHAR(255) DEFAULT NULL, user_delete VARCHAR(255) DEFAULT NULL, date_create DATETIME DEFAULT NULL, date_update DATETIME DEFAULT NULL, date_delete DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE prise_service (id INT AUTO_INCREMENT NOT NULL, ligne_depense_prise_service_id_id INT DEFAULT NULL, date_debut DATETIME NOT NULL, date_fin DATETIME NOT NULL, user_create VARCHAR(255) DEFAULT NULL, user_update VARCHAR(255) DEFAULT NULL, user_delete VARCHAR(255) DEFAULT NULL, date_create DATETIME DEFAULT NULL, date_update DATETIME DEFAULT NULL, date_delete DATETIME DEFAULT NULL, INDEX IDX_AD15D366C6217D13 (ligne_depense_prise_service_id_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE promotion (id INT AUTO_INCREMENT NOT NULL, nom_promotion VARCHAR(255) DEFAULT NULL, description VARCHAR(255) DEFAULT NULL, pourcentage DOUBLE PRECISION NOT NULL, date_debut DATETIME NOT NULL, date_fin DATETIME NOT NULL, user_create VARCHAR(255) DEFAULT NULL, user_update VARCHAR(255) DEFAULT NULL, user_delete VARCHAR(255) DEFAULT NULL, date_create DATETIME DEFAULT NULL, date_update DATETIME DEFAULT NULL, date_delete DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE rendez_vous (id INT AUTO_INCREMENT NOT NULL, statut VARCHAR(255) DEFAULT NULL, commentaire VARCHAR(255) DEFAULT NULL, date_rendez_vous DATETIME NOT NULL, user_create VARCHAR(255) DEFAULT NULL, user_update VARCHAR(255) DEFAULT NULL, user_delete VARCHAR(255) DEFAULT NULL, date_create DATETIME DEFAULT NULL, date_update DATETIME DEFAULT NULL, date_delete DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE type_acquisition (id INT AUTO_INCREMENT NOT NULL, acquisition_type_acquisition_id_id INT DEFAULT NULL, description VARCHAR(255) NOT NULL, user_create VARCHAR(255) DEFAULT NULL, user_update VARCHAR(255) DEFAULT NULL, user_delete VARCHAR(255) DEFAULT NULL, date_create DATETIME DEFAULT NULL, date_update DATETIME DEFAULT NULL, date_delete DATETIME DEFAULT NULL, INDEX IDX_64F247A27A86CCA5 (acquisition_type_acquisition_id_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE type_depense (id INT AUTO_INCREMENT NOT NULL, type_depense_id_id INT DEFAULT NULL, id_type_depense_id INT DEFAULT NULL, user_create VARCHAR(255) DEFAULT NULL, user_update VARCHAR(255) DEFAULT NULL, user_delete VARCHAR(255) DEFAULT NULL, date_create DATETIME DEFAULT NULL, date_update DATETIME DEFAULT NULL, date_delete DATETIME DEFAULT NULL, INDEX IDX_1C24F8A2E380BBCF (type_depense_id_id), INDEX IDX_1C24F8A2B58500CE (id_type_depense_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE type_immobilier (id INT AUTO_INCREMENT NOT NULL, immobilier_type_immobilier_id_id INT DEFAULT NULL, nom_type_immobilier VARCHAR(255) DEFAULT NULL, description VARCHAR(255) DEFAULT NULL, user_create VARCHAR(255) DEFAULT NULL, user_update VARCHAR(255) DEFAULT NULL, user_delete VARCHAR(255) DEFAULT NULL, date_create DATETIME DEFAULT NULL, date_update DATETIME DEFAULT NULL, date_delete DATETIME DEFAULT NULL, INDEX IDX_92EF2F779F1E9C (immobilier_type_immobilier_id_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE type_paiement (id INT AUTO_INCREMENT NOT NULL, moyen_paiement_type_paiement_id_id INT DEFAULT NULL, nom_type_paiement VARCHAR(255) DEFAULT NULL, user_create VARCHAR(255) DEFAULT NULL, user_update VARCHAR(255) DEFAULT NULL, user_delete VARCHAR(255) DEFAULT NULL, date_create DATETIME DEFAULT NULL, date_update DATETIME DEFAULT NULL, date_delete DATETIME DEFAULT NULL, INDEX IDX_7C235DE2BAEC6E6C (moyen_paiement_type_paiement_id_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE `user` (id INT AUTO_INCREMENT NOT NULL, adresse_id INT DEFAULT NULL, adresse_user_id_id INT DEFAULT NULL, notification_user_id_id INT DEFAULT NULL, username VARCHAR(180) NOT NULL, roles JSON NOT NULL COMMENT \'(DC2Type:json)\', password VARCHAR(255) NOT NULL, telephone VARCHAR(255) DEFAULT NULL, nom VARCHAR(255) DEFAULT NULL, prenom VARCHAR(255) DEFAULT NULL, user_create VARCHAR(255) DEFAULT NULL, user_update VARCHAR(255) DEFAULT NULL, user_delete VARCHAR(255) DEFAULT NULL, date_create DATETIME DEFAULT NULL, date_update DATETIME DEFAULT NULL, date_delete DATETIME DEFAULT NULL, INDEX IDX_8D93D6494DE7DC5C (adresse_id), INDEX IDX_8D93D649F29B1578 (adresse_user_id_id), INDEX IDX_8D93D64966F4C02D (notification_user_id_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE messenger_messages (id BIGINT AUTO_INCREMENT NOT NULL, body LONGTEXT NOT NULL, headers LONGTEXT NOT NULL, queue_name VARCHAR(190) NOT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', available_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', delivered_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX IDX_75EA56E0FB7336F0 (queue_name), INDEX IDX_75EA56E0E3BD61CE (available_at), INDEX IDX_75EA56E016BA31DB (delivered_at), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE adresse ADD CONSTRAINT FK_C35F0816EC52F3F0 FOREIGN KEY (adresse_client_id_id) REFERENCES client (id)');
        $this->addSql('ALTER TABLE adresse ADD CONSTRAINT FK_C35F081688E44FC0 FOREIGN KEY (adresse_agent_id_id) REFERENCES agent (id)');
        $this->addSql('ALTER TABLE adresse ADD CONSTRAINT FK_C35F0816113403A1 FOREIGN KEY (immobilier_adresse_id_id) REFERENCES immobilier (id)');
        $this->addSql('ALTER TABLE agent ADD CONSTRAINT FK_268B9C9DA148D5EF FOREIGN KEY (agent_prise_service_id_id) REFERENCES prise_service (id)');
        $this->addSql('ALTER TABLE categorie ADD CONSTRAINT FK_497DD634E87A0BAA FOREIGN KEY (immobilier_categorie_id_id) REFERENCES immobilier (id)');
        $this->addSql('ALTER TABLE client ADD CONSTRAINT FK_C744045597F70B29 FOREIGN KEY (contrat_client_id_id) REFERENCES contrat (id)');
        $this->addSql('ALTER TABLE client ADD CONSTRAINT FK_C7440455EA5C1F0A FOREIGN KEY (acquisition_client_id_id) REFERENCES acquisition (id)');
        $this->addSql('ALTER TABLE client ADD CONSTRAINT FK_C744045588F0D212 FOREIGN KEY (facture_client_id_id) REFERENCES facture (id)');
        $this->addSql('ALTER TABLE client ADD CONSTRAINT FK_C744045537B4C4A1 FOREIGN KEY (immobilier_client_id_id) REFERENCES immobilier (id)');
        $this->addSql('ALTER TABLE contrat ADD CONSTRAINT FK_603499938E65F6F0 FOREIGN KEY (ligne_contrat_id_id) REFERENCES ligne_contrat (id)');
        $this->addSql('ALTER TABLE contrat ADD CONSTRAINT FK_60349993D6E1BD71 FOREIGN KEY (facture_contrat_id_id) REFERENCES facture (id)');
        $this->addSql('ALTER TABLE depense ADD CONSTRAINT FK_34059757547300E6 FOREIGN KEY (ligne_depense_depense_id_id) REFERENCES ligne_depense (id)');
        $this->addSql('ALTER TABLE duree_contrat ADD CONSTRAINT FK_8BFD47C48E005BF FOREIGN KEY (contrat_duree_contrat_id_id) REFERENCES contrat (id)');
        $this->addSql('ALTER TABLE immobilier ADD CONSTRAINT FK_142D24D22D9DD0EE FOREIGN KEY (contrat_immobilier_id_id) REFERENCES contrat (id)');
        $this->addSql('ALTER TABLE immobilier ADD CONSTRAINT FK_142D24D238C55694 FOREIGN KEY (promotion_immobilier_id_id) REFERENCES promotion (id)');
        $this->addSql('ALTER TABLE prise_service ADD CONSTRAINT FK_AD15D366C6217D13 FOREIGN KEY (ligne_depense_prise_service_id_id) REFERENCES ligne_depense (id)');
        $this->addSql('ALTER TABLE type_acquisition ADD CONSTRAINT FK_64F247A27A86CCA5 FOREIGN KEY (acquisition_type_acquisition_id_id) REFERENCES acquisition (id)');
        $this->addSql('ALTER TABLE type_depense ADD CONSTRAINT FK_1C24F8A2E380BBCF FOREIGN KEY (type_depense_id_id) REFERENCES depense (id)');
        $this->addSql('ALTER TABLE type_depense ADD CONSTRAINT FK_1C24F8A2B58500CE FOREIGN KEY (id_type_depense_id) REFERENCES depense (id)');
        $this->addSql('ALTER TABLE type_immobilier ADD CONSTRAINT FK_92EF2F779F1E9C FOREIGN KEY (immobilier_type_immobilier_id_id) REFERENCES immobilier (id)');
        $this->addSql('ALTER TABLE type_paiement ADD CONSTRAINT FK_7C235DE2BAEC6E6C FOREIGN KEY (moyen_paiement_type_paiement_id_id) REFERENCES moyen_paiement (id)');
        $this->addSql('ALTER TABLE `user` ADD CONSTRAINT FK_8D93D6494DE7DC5C FOREIGN KEY (adresse_id) REFERENCES adresse (id)');
        $this->addSql('ALTER TABLE `user` ADD CONSTRAINT FK_8D93D649F29B1578 FOREIGN KEY (adresse_user_id_id) REFERENCES agent (id)');
        $this->addSql('ALTER TABLE `user` ADD CONSTRAINT FK_8D93D64966F4C02D FOREIGN KEY (notification_user_id_id) REFERENCES notification (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE adresse DROP FOREIGN KEY FK_C35F0816EC52F3F0');
        $this->addSql('ALTER TABLE adresse DROP FOREIGN KEY FK_C35F081688E44FC0');
        $this->addSql('ALTER TABLE adresse DROP FOREIGN KEY FK_C35F0816113403A1');
        $this->addSql('ALTER TABLE agent DROP FOREIGN KEY FK_268B9C9DA148D5EF');
        $this->addSql('ALTER TABLE categorie DROP FOREIGN KEY FK_497DD634E87A0BAA');
        $this->addSql('ALTER TABLE client DROP FOREIGN KEY FK_C744045597F70B29');
        $this->addSql('ALTER TABLE client DROP FOREIGN KEY FK_C7440455EA5C1F0A');
        $this->addSql('ALTER TABLE client DROP FOREIGN KEY FK_C744045588F0D212');
        $this->addSql('ALTER TABLE client DROP FOREIGN KEY FK_C744045537B4C4A1');
        $this->addSql('ALTER TABLE contrat DROP FOREIGN KEY FK_603499938E65F6F0');
        $this->addSql('ALTER TABLE contrat DROP FOREIGN KEY FK_60349993D6E1BD71');
        $this->addSql('ALTER TABLE depense DROP FOREIGN KEY FK_34059757547300E6');
        $this->addSql('ALTER TABLE duree_contrat DROP FOREIGN KEY FK_8BFD47C48E005BF');
        $this->addSql('ALTER TABLE immobilier DROP FOREIGN KEY FK_142D24D22D9DD0EE');
        $this->addSql('ALTER TABLE immobilier DROP FOREIGN KEY FK_142D24D238C55694');
        $this->addSql('ALTER TABLE prise_service DROP FOREIGN KEY FK_AD15D366C6217D13');
        $this->addSql('ALTER TABLE type_acquisition DROP FOREIGN KEY FK_64F247A27A86CCA5');
        $this->addSql('ALTER TABLE type_depense DROP FOREIGN KEY FK_1C24F8A2E380BBCF');
        $this->addSql('ALTER TABLE type_depense DROP FOREIGN KEY FK_1C24F8A2B58500CE');
        $this->addSql('ALTER TABLE type_immobilier DROP FOREIGN KEY FK_92EF2F779F1E9C');
        $this->addSql('ALTER TABLE type_paiement DROP FOREIGN KEY FK_7C235DE2BAEC6E6C');
        $this->addSql('ALTER TABLE `user` DROP FOREIGN KEY FK_8D93D6494DE7DC5C');
        $this->addSql('ALTER TABLE `user` DROP FOREIGN KEY FK_8D93D649F29B1578');
        $this->addSql('ALTER TABLE `user` DROP FOREIGN KEY FK_8D93D64966F4C02D');
        $this->addSql('DROP TABLE acquisition');
        $this->addSql('DROP TABLE adresse');
        $this->addSql('DROP TABLE agent');
        $this->addSql('DROP TABLE categorie');
        $this->addSql('DROP TABLE client');
        $this->addSql('DROP TABLE contrat');
        $this->addSql('DROP TABLE depense');
        $this->addSql('DROP TABLE duree_contrat');
        $this->addSql('DROP TABLE facture');
        $this->addSql('DROP TABLE immobilier');
        $this->addSql('DROP TABLE ligne_contrat');
        $this->addSql('DROP TABLE ligne_depense');
        $this->addSql('DROP TABLE moyen_paiement');
        $this->addSql('DROP TABLE notification');
        $this->addSql('DROP TABLE prise_service');
        $this->addSql('DROP TABLE promotion');
        $this->addSql('DROP TABLE rendez_vous');
        $this->addSql('DROP TABLE type_acquisition');
        $this->addSql('DROP TABLE type_depense');
        $this->addSql('DROP TABLE type_immobilier');
        $this->addSql('DROP TABLE type_paiement');
        $this->addSql('DROP TABLE `user`');
        $this->addSql('DROP TABLE messenger_messages');
    }
}
