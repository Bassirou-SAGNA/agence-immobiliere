<?php

namespace App\Entity;

use App\Repository\ContratRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ContratRepository::class)]
class Contrat
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(type: 'float', nullable: true)]
    private ?float $Montant = null;

    #[ORM\Column(length: 255)]
    private ?string $Nom_Contrat = null;

    #[ORM\Column(length: 255)]
    private ?string $Description = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTimeInterface $DateContrat = null;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $user_create = null;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $user_update = null;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $user_delete = null;

    #[ORM\Column(type: 'datetime', nullable: true)]
    private ?\DateTimeInterface $date_create = null;

    #[ORM\Column(type: 'datetime', nullable: true)]
    private ?\DateTimeInterface $date_update = null;

    #[ORM\Column(type: 'datetime', nullable: true)]
    private ?\DateTimeInterface $date_delete = null;

    #[ORM\ManyToOne]
    #[ORM\JoinColumn(nullable: true)]
    private ?DureeContrat $dureeContrat=null;

    #[ORM\ManyToOne]
    #[ORM\JoinColumn(nullable: true)]
    private ?BienImmobilier $bienImmobilier=null;

    #[ORM\ManyToOne]
    #[ORM\JoinColumn(nullable: true)]
    private ?Client $client=null;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMontant(): ?float
    {
        return $this->Montant;
    }

    public function setMontant(?float $Montant): self
    {
        $this->Montant = $Montant;

        return $this;
    }

    public function getNomContrat(): ?string
    {
        return $this->Nom_Contrat;
    }

    public function setNomContrat(string $Nom_Contrat): self
    {
        $this->Nom_Contrat = $Nom_Contrat;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->Description;
    }

    public function setDescription(string $Description): self
    {
        $this->Description = $Description;

        return $this;
    }

    public function getDateContrat(): ?\DateTimeInterface
    {
        return $this->DateContrat;
    }

    public function setDateContrat(\DateTimeInterface $DateContrat): self
    {
        $this->DateContrat = $DateContrat;

        return $this;
    }

    public function getUserCreate(): ?string
    {
        return $this->user_create;
    }

    public function setUserCreate(?string $user_create): self
    {
        $this->user_create = $user_create;

        return $this;
    }

    public function getUserUpdate(): ?string
    {
        return $this->user_update;
    }

    public function setUserUpdate(?string $user_update): self
    {
        $this->user_update = $user_update;

        return $this;
    }

    public function getUserDelete(): ?string
    {
        return $this->user_delete;
    }

    public function setUserDelete(?string $user_delete): self
    {
        $this->user_delete = $user_delete;

        return $this;
    }

    public function getDateCreate(): ?\DateTimeInterface
    {
        return $this->date_create;
    }

    public function setDateCreate(?\DateTimeInterface $date_create): self
    {
        $this->date_create = $date_create;

        return $this;
    }

    public function getDateUpdate(): ?\DateTimeInterface
    {
        return $this->date_update;
    }

    public function setDateUpdate(?\DateTimeInterface $date_update): self
    {
        $this->date_update = $date_update;

        return $this;
    }

    public function getDateDelete(): ?\DateTimeInterface
    {
        return $this->date_delete;
    }

    public function setDateDelete(?\DateTimeInterface $date_delete): self
    {
        $this->date_delete = $date_delete;

        return $this;
    }

    public function getDureeContrat(): ?dureeContrat
    {
        return $this->dureeContrat;
    }
public function setDureeContrat(?DureeContrat $dureeContrat):static
{
    $this->dureeContrat = $dureeContrat;

    return $this;
}

    public function getBienImmobilier():?BienImmobilier
    {
        return $this->bienImmobilier;
    }
    public function setBienImmobilier(?BienImmobilier $bienImmobilier): static{
        $this->bienImmobilier = $bienImmobilier;
        return $this;
    }
    public function getClient():?Client{
        return $this->client;
    }
    public function setClient(?Client $client): static{
        $this->client = $client;
        return $this;
    }

}
