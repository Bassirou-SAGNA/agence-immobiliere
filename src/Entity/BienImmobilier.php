<?php
namespace App\Entity;

use App\Repository\BienImmobilierRepository;
use Symfony\Component\Validator\Constraints as Assert;
use App\Entity\RendezVous;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: BienImmobilierRepository::class)]
class BienImmobilier
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(type: Types::TEXT, nullable:true)]
    private ?string $description = null;

    #[ORM\Column]
    private ?float $prix = null;

    #[ORM\Column]
    private ?int $superficie = null;

    #[ORM\Column(nullable:true)]
    private ?int $nombrePieces = null;

    #[ORM\ManyToOne]
    #[ORM\JoinColumn(nullable: true)]
    private ?Adresse $adresse = null;

    #[ORM\ManyToOne]
    #[ORM\JoinColumn]
    private ?Categorie $categorie = null;

    #[ORM\ManyToOne]
    #[ORM\JoinColumn(nullable: true)]
    private ?TypeImmobilier $typeImmobilier = null;

    #[ORM\Column(type: 'string', length: 255, unique: true)]
    private ?string $nomImmobilier = null;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $user_create;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $user_update;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $user_delete;

    #[ORM\Column(type: 'datetime', nullable: true)]
    private $date_create;

    #[ORM\Column(type: 'datetime', nullable: true)]
    private $date_update;

    #[ORM\Column(type: 'datetime', nullable: true)]
    private $date_delete;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): static
    {
        $this->description = $description;

        return $this;
    }

    public function getPrix(): ?float
    {
        return $this->prix;
    }

    public function setPrix(float $prix): static
    {
        $this->prix = $prix;

        return $this;
    }

    public function getSuperficie(): ?int
{
    return $this->superficie;
}

public function setSuperficie(int $superficie): static
{
    $this->superficie = $superficie;

    return $this;
}


public function getNombrePieces(): ?int
{
    return $this->nombrePieces;
}

public function setNombrePieces(?int $nombrePieces): static
{
    $this->nombrePieces = $nombrePieces;

    return $this;
}

    public function getAdresse():?Adresse
    {
        return $this->adresse;
    }
    public function setAdresse(?Adresse $adresse): static
    {
        $this->adresse = $adresse;
        return $this;
    }
    public function getCategorie():?Categorie
    {
        return $this->categorie;
    }
    public function setCategorie(?Categorie $categorie): static
    {
        $this->categorie = $categorie;
        return $this;
    }
    public function getTypeImmobilier():?TypeImmobilier
    {
        return $this->typeImmobilier;
    }
    public function setTypeImmobilier(?TypeImmobilier $typeImmobilier): static
    {
        $this->typeImmobilier = $typeImmobilier;
        return $this;
    }
    public function getUserCreate(): ?string
    {
        return $this->user_create;
    }

    public function setUserCreate(?string $user_create): static
    {
        $this->user_create = $user_create;

        return $this;
    }

    public function getUserUpdate(): ?string
    {
        return $this->user_update;
    }

    public function setUserUpdate(?string $user_update): static
    {
        $this->user_update = $user_update;

        return $this;
    }

    public function getUserDelete(): ?string
    {
        return $this->user_delete;
    }

    public function setUserDelete(?string $user_delete): static
    {
        $this->user_delete = $user_delete;

        return $this;
    }

    public function getDateCreate(): ?\DateTimeInterface
    {
        return $this->date_create;
    }

    public function setDateCreate(?\DateTimeInterface $date_create): static
    {
        $this->date_create = $date_create;

        return $this;
    }

    public function getDateUpdate(): ?\DateTimeInterface
    {
        return $this->date_update;
    }

    public function setDateUpdate(?\DateTimeInterface $date_update): static
    {
        $this->date_update = $date_update;

        return $this;
    }

    public function getDateDelete(): ?\DateTimeInterface
    {
        return $this->date_delete;
    }

    public function setDateDelete(?\DateTimeInterface $date_delete): static
    {
        $this->date_delete = $date_delete;

        return $this;
    }
    public function getNomImmobilier():?string{
        return $this->nomImmobilier;
    }
    public function setNomImmobilier(?string $nomImmobilier): static{
        $this->nomImmobilier = $nomImmobilier;
        return $this;
    }
}
