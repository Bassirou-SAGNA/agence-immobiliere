<?php

namespace App\Controller;

use App\Entity\TypeImmobilier;
use App\Form\TypeImmobilierType;
use App\Repository\TypeImmobilierRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

#[Route('/type/immobilier')]
class TypeImmobilierController extends AbstractController
{
    #[Route('/', name: 'app_type_immobilier_index', methods: ['GET'])]
    public function index(TypeImmobilierRepository $typeImmobilierRepository): Response
    {
        return $this->render('type_immobilier/index.html.twig', [
            'type_immobiliers' => $typeImmobilierRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_type_immobilier_new', methods: ['GET', 'POST'])]
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $typeImmobilier = new TypeImmobilier();
        $form = $this->createForm(TypeImmobilierType::class, $typeImmobilier);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($typeImmobilier);
            $entityManager->flush();

            return $this->redirectToRoute('app_type_immobilier_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('type_immobilier/new.html.twig', [
            'type_immobilier' => $typeImmobilier,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_type_immobilier_show', methods: ['GET'])]
    public function show(TypeImmobilier $typeImmobilier): Response
    {
        return $this->render('type_immobilier/show.html.twig', [
            'type_immobilier' => $typeImmobilier,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_type_immobilier_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, TypeImmobilier $typeImmobilier, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(TypeImmobilierType::class, $typeImmobilier);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('app_type_immobilier_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('type_immobilier/edit.html.twig', [
            'type_immobilier' => $typeImmobilier,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_type_immobilier_delete', methods: ['POST'])]
    public function delete(Request $request, TypeImmobilier $typeImmobilier, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$typeImmobilier->getId(), $request->getPayload()->getString('_token'))) {
            $entityManager->remove($typeImmobilier);
            $entityManager->flush();
        }

        return $this->redirectToRoute('app_type_immobilier_index', [], Response::HTTP_SEE_OTHER);
    }
}
