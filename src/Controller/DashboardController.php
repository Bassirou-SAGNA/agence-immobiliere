<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractController
{
    /**
     * @Route("/dashboard", name="dashboard")
     */
    public function index(): Response
    {
        // Exemples de données statiques, remplacez par des requêtes à votre base de données
        $total_proprietaires = 50;
        $total_biens = 200;
        $total_clients = 150;
        $total_visites = 75;

        return $this->render('dashboard/index.html.twig', [
            'total_proprietaires' => $total_proprietaires,
            'total_biens' => $total_biens,
            'total_clients' => $total_clients,
            'total_visites' => $total_visites,
        ]);
    }
}
