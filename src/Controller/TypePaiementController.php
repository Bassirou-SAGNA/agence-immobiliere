<?php

namespace App\Controller;

use App\Entity\TypePaiement;
use App\Form\TypePaiementType;
use App\Repository\TypePaiementRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

#[Route('/type/paiement')]
class TypePaiementController extends AbstractController
{
    #[Route('/', name: 'app_type_paiement_index', methods: ['GET'])]
    public function index(TypePaiementRepository $typePaiementRepository): Response
    {
        return $this->render('type_paiement/index.html.twig', [
            'type_paiements' => $typePaiementRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_type_paiement_new', methods: ['GET', 'POST'])]
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $typePaiement = new TypePaiement();
        $form = $this->createForm(TypePaiementType::class, $typePaiement);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($typePaiement);
            $entityManager->flush();

            return $this->redirectToRoute('app_type_paiement_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('type_paiement/new.html.twig', [
            'type_paiement' => $typePaiement,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_type_paiement_show', methods: ['GET'])]
    public function show(TypePaiement $typePaiement): Response
    {
        return $this->render('type_paiement/show.html.twig', [
            'type_paiement' => $typePaiement,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_type_paiement_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, TypePaiement $typePaiement, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(TypePaiementType::class, $typePaiement);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('app_type_paiement_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('type_paiement/edit.html.twig', [
            'type_paiement' => $typePaiement,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_type_paiement_delete', methods: ['POST'])]
    public function delete(Request $request, TypePaiement $typePaiement, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$typePaiement->getId(), $request->getPayload()->getString('_token'))) {
            $entityManager->remove($typePaiement);
            $entityManager->flush();
        }

        return $this->redirectToRoute('app_type_paiement_index', [], Response::HTTP_SEE_OTHER);
    }
}
