<?php

namespace App\Controller;

use App\Entity\DureeContrat;
use App\Form\DureeContrat1Type;
use App\Repository\DureeContratRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

#[Route('/duree/contrat')]
class DureeContratController extends AbstractController
{
    #[Route('/', name: 'app_duree_contrat_index', methods: ['GET'])]
    public function index(DureeContratRepository $dureeContratRepository): Response
    {
        return $this->render('duree_contrat/index.html.twig', [
            'duree_contrats' => $dureeContratRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_duree_contrat_new', methods: ['GET', 'POST'])]
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $dureeContrat = new DureeContrat();
        $form = $this->createForm(DureeContrat1Type::class, $dureeContrat);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($dureeContrat);
            $entityManager->flush();

            return $this->redirectToRoute('app_duree_contrat_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('duree_contrat/new.html.twig', [
            'duree_contrat' => $dureeContrat,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_duree_contrat_show', methods: ['GET'])]
    public function show(DureeContrat $dureeContrat): Response
    {
        return $this->render('duree_contrat/show.html.twig', [
            'duree_contrat' => $dureeContrat,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_duree_contrat_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, DureeContrat $dureeContrat, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(DureeContrat1Type::class, $dureeContrat);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('app_duree_contrat_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('duree_contrat/edit.html.twig', [
            'duree_contrat' => $dureeContrat,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_duree_contrat_delete', methods: ['POST'])]
    public function delete(Request $request, DureeContrat $dureeContrat, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$dureeContrat->getId(), $request->getPayload()->getString('_token'))) {
            $entityManager->remove($dureeContrat);
            $entityManager->flush();
        }

        return $this->redirectToRoute('app_duree_contrat_index', [], Response::HTTP_SEE_OTHER);
    }
}
