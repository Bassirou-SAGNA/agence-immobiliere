<?php

namespace App\Form;

use App\Entity\Adresse;
use App\Entity\Agent;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AgentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('nom')
            ->add('prenom')
            ->add('numeroTelephone')
            // ->add('user_create')
            // ->add('user_update')
            // ->add('user_delete')
            // ->add('date_create', null, [
            //     'widget' => 'single_text',
            // ])
            // ->add('date_update', null, [
            //     'widget' => 'single_text',
            // ])
            // ->add('date_delete', null, [
            //     'widget' => 'single_text',
            // ])
            ->add('adresse', EntityType::class, [
                'class' => Adresse::class,
                'choice_label' => ' ville',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Agent::class,
        ]);
    }
}
