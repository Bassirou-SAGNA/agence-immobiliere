<?php

namespace App\Form;

use App\Entity\Adresse;
use App\Entity\BienImmobilier;
use App\Entity\Categorie;
use App\Entity\TypeImmobilier;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BienImmobilier1Type extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('description')
            ->add('prix')
            ->add('superficie')
            ->add('nombrePieces')
            ->add('nomImmobilier')
            // ->add('user_create')
            // ->add('user_update')
            // ->add('user_delete')
            // ->add('date_create', null, [
            //     'widget' => 'single_text',
            // ])
            // ->add('date_update', null, [
            //     'widget' => 'single_text',
            // ])
            // ->add('date_delete', null, [
            //     'widget' => 'single_text',
            // ])
            ->add('adresse', EntityType::class, [
                'class' => Adresse::class,
                'choice_label' => 'ville',
            ])
            ->add('categorie', EntityType::class, [
                'class' => Categorie::class,
                'choice_label' => 'nomCategorie',
            ])
            ->add('typeImmobilier', EntityType::class, [
                'class' => TypeImmobilier::class,
                'choice_label' => 'nomTypeImmobilier',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => BienImmobilier::class,
        ]);
    }
}
