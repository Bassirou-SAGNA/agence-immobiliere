<?php

namespace App\Form;

use App\Entity\BienImmobilier;
use App\Entity\Client;
use App\Entity\Contrat;
use App\Entity\DureeContrat;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ContratType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('Montant')
            ->add('Nom_Contrat')
            ->add('Description')
            ->add('DateContrat', null, [
                'widget' => 'single_text',
            ])
            // ->add('user_create')
            // ->add('user_update')
            // ->add('user_delete')
            // ->add('date_create', null, [
            //     'widget' => 'single_text',
            // ])
            // ->add('date_update', null, [
            //     'widget' => 'single_text',
            // ])
            // ->add('date_delete', null, [
            //     'widget' => 'single_text',
            // ])
            ->add('dureeContrat', EntityType::class, [
                'class' => DureeContrat::class,
                'choice_label' => 'id',
            ])
            ->add('bienImmobilier', EntityType::class, [
                'class' => BienImmobilier::class,
                'choice_label' => 'nomImmobilier',
            ])
            ->add('client', EntityType::class, [
                'class' => Client::class,
                'choice_label' => 'id',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Contrat::class,
        ]);
    }
}
