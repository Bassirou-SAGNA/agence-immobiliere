<?php

namespace App\Form;

use App\Entity\MoyenPaiement;
use App\Entity\TypePaiement;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MoyenPaiementType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('Nom_Moyen_Paiement')
            ->add('Frais')
            // ->add('user_create')
            // ->add('user_update')
            // ->add('user_delete')
            // ->add('date_create', null, [
            //     'widget' => 'single_text',
            // ])
            // ->add('date_update', null, [
            //     'widget' => 'single_text',
            // ])
            // ->add('date_delete', null, [
            //     'widget' => 'single_text',
            // ])
            ->add('typePaiement', EntityType::class, [
                'class' => TypePaiement::class,
                'choice_label' => 'Nom_Type_Paiement',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => MoyenPaiement::class,
        ]);
    }
}
