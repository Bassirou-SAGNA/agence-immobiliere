<?php

namespace App\Form;

use App\Entity\TypeImmobilier;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TypeImmobilierType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('nomTypeImmobilier')
            // ->add('user_create')
            // ->add('user_update')
            // ->add('user_delete')
            // ->add('date_create', null, [
            //     'widget' => 'single_text',
            // ])
            // ->add('date_update', null, [
            //     'widget' => 'single_text',
            // ])
            // ->add('date_delete', null, [
            //     'widget' => 'single_text',
            // ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => TypeImmobilier::class,
        ]);
    }
}
