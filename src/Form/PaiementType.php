<?php

namespace App\Form;

use App\Entity\MoyenPaiement;
use App\Entity\Paiement;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PaiementType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('datePaiement', null, [
                'widget' => 'single_text',
            ])
            ->add('moyenPaiement', EntityType::class, [
                'class' => MoyenPaiement::class,
                'choice_label' => 'Nom_Moyen_Paiement',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Paiement::class,
        ]);
    }
}
