        // JavaScript pour le toggle du sidebar
        document.addEventListener('DOMContentLoaded', () => {
            const menuBar = document.querySelector('#content nav .bx.bx-menu');
            const sidebar = document.getElementById('sidebar');
            const content = document.getElementById('content');

            menuBar.addEventListener('click', () => {
                sidebar.classList.toggle('hide');
                // Adjust the content width and position
                if (sidebar.classList.contains('hide')) {
                    content.style.width = 'calc(100% - 60px)';
                    content.style.left = '60px';
                } else {
                    content.style.width = 'calc(100% - 280px)';
                    content.style.left = '280px';
                }
            });
        });
